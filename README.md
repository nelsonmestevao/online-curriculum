# Online Curriculum

This is my Online Curriculum Vitae.

## Credits

This theme is designed by Xiaoying Riley at [3rd Wave Media](http://themes.3rdwavemedia.com/). Visit her [website](http://themes.3rdwavemedia.com/) for more themes. [Sharath Kumar](https://github.com/sharu725) made this into a Jekyll Theme.
